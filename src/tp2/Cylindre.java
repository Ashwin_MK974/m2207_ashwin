package tp2;

public class Cylindre extends Cercle
{
	
	/***************Exercice 3.1 ************/
	double hauteur ; 
	// Cr�ation du constructeur 
	public Cylindre()
	{
		hauteur = 1.0 ; // Initialisation de la haueteur � une valeur double de 1.0  
	}
	
	//Ajout des accesseurs 
	double getHauteur() // Pour obtenir hauteur
	{
		return hauteur; 
	}
	
	void setHauteur(double h) // Pour pouvoir modifier la  hauetur  du cylindree 
	{
		hauteur = h ; 
	}
	
	//R�definitions de la m�thode se d�crire 
	 String seDecrire()
	 {
		                            
			return (" un cylindre de hauteur " + hauteur +  " est issue d'" + super.seDecrire());   // Ajout de la phrase + ajout de la phrase de pr�sentation
																								 // de  la m�thode de la classe M�re "Forme" avec super.seDecrire ; 		
			
	 }
	/***************Fin Exercice 3.1 ************/
	 
	/***************Exercice 3.3 ************/
	 public Cylindre(double h, double r,String couleur, boolean coloriage)
	 {
		 super(r,couleur,coloriage );  // Va chercher le rayon dans cercle dont couleur et coloriage sont  trouv�es dans Forme ; 
		 hauteur = h ;  // hauteur = h  pour d�ninir le param�tre de la hauteur du cylindre lors de la cr�ation de lobjet
		 
	 }	
	 /***************Fin Exercice 3.3 ************/
	 
	 /***************Exercice 3.5  ***************/ //M�thode de calcule volume cylindre   //Le volume du cylindre �quivaut � pi * r^2 * h 
	 double calculevolumeCylindre()
	 	{
	 		return Math.PI * Math.pow(super.getRayon() , 2)*  hauteur ; //super.getRayon pour obtenir la valeur du rayon depuis la classe Mere cercle
	 		
	 	}
	 /***************Fin Exercice 3.5  ************/
	 
}
