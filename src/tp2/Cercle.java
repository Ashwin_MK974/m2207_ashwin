package tp2;

public class Cercle extends Forme // Classe Cercle

{
	
		/*************** Exercice 2.1************/	
		private double rayon; 
		/***************Fin Exercice 2.1************/
	
		/*************** Exercice 2.2************/
		Cercle()
		{
			super()  ;    // Appele le constructeur sans argument ce la classe Forme
			rayon = 1.0 ; //Initialise le rayon  � une valeur de 1.0  
		}
		/*************** Fin Exercice 2.2 ************/	
	
		/*************** Exercice 2.3************/        //Ajout des accesseurs pour retourner la valeur et modifier la valeur de l'attribut rayon 
		double getRayon() // Accesseur pour obtenir la valeur du rayon 
		{
			return rayon ; 
		}
		void setRayon(double r) // Accessseur pour modifier la valeur du rayon ; 
		{
			rayon = r ; 
		}
		/***************Fin  Exercice 2.3************/    	
		
		/***************Exercice 2.6************/    		
		String seDecrire()                                
		{
				 																				
			return (" un Cercle de rayon " + rayon +  " est issue d'" + super.seDecrire());   // Ajout de la phrase + ajout de la phrase de pr�sentation
																							 // de  la m�thode de la classe M�re "Forme" avec super.seDecrire ; 		
		}
		/*************** Fin Exercice 2.6***********/  
		
		/***************Exercice 2.7************/        //Ajout du constructeur
		Cercle(double r)
			  {
				rayon = r  ;  
			  }
		/***************Fin Exercice 2.7************/    
		
		/***************Exercice 2.9************/  // Ajout du nouveau constructeurr 
	   Cercle(double r,String couleur, boolean coloriage)
			 {
				super(couleur,coloriage) ; // Appel au param�tre du constructeur (2eme constructeur de la classe M�re)
				rayon = r ;  // rayon = r pour d�ninir le param�tre du rayon lors de la cr�ation de lobjet
								
			 }
		/***************Fin Exercice 2.9************/
	   
	   /***************Exercice 2.11************/ // Ajout de la m�thode calucle d'aire et de p�rimetre 
	   double calculerAire()   // Aire d'un cercle est calcul� par (Rayon)� * pi soit r� * pi 
	   {
		   return Math.pow(rayon, 2) * Math.PI ; // Math.pow pour la puissance 2 ici  rayon � la puissance 2 soit math.pow(x,y) avec x= rayon y = 2 pour rayon^2 
	   }										// On multiplie ensuite par pi ce qui �quivaut � un r�sultat de type double
	   
	   double calculerPerimetre()  //  P�rimetre d'un cercle est calcul� par P = 2 * pi * r 
	   {
		   return 2 * Math.PI * rayon ; 
	   }
	 
	   
	   /***************Fin Exercice 2.11************/
	   
	   
	   
}
