package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.*;
import java.net.ServerSocket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
public class PanneauServer extends JFrame implements ActionListener{

	//Attributs
	JButton button = new JButton("Exit"); 
	JTextArea texte= new JTextArea("Le panneau est actif");
	JScrollPane s = new JScrollPane(texte);
	//Constructeurs
    public PanneauServer () 
    { 
		super();
		this.setTitle("Serveur - Panneau d'affichage"); 
		this.setSize(390,300); 
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		Container panneau = getContentPane(); 
		panneau.add(button, BorderLayout.SOUTH);
		panneau.add(s);
		button.addActionListener(this);

		System.out.println(" Le panneau est actif !"); 		
		this.setVisible(true);

		ServerSocket monServerSocket;
		texte.append("\n Le  Serveura  d�marrer ");
		try {
			monServerSocket = new ServerSocket(8888);
			System.out.println("ServerSocket: " + monServerSocket);
			monServerSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actionPerformed (ActionEvent e){ 
		System.out.println(" Une action a �t� d�tecter ");
		System.exit(-1);
	}
	public void ecouter()
	{
		System.out.println(" Server en attente de connexion");
	}
	
	public static void main(String[] args){
		PanneauServer p = new PanneauServer ();
		}
}