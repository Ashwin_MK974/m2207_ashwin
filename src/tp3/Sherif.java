package tp3;

public class Sherif extends Cowboy

{/***********Exercice 8.1***********/
	private  int  popular  ; 
	private String caracteristique ; 
	//D�but du constructeur 
	public Sherif(String n) 
	{
		super(n);          										//Le sh�rif est ici un humain caracteris� par un nom 
		caracteristique = " Vaillant " ; 
	}
	  int stopBrigand  ; 										//Nombre de brigand que le sh�rif  � arret� 
	// Fin du constructeur 	
	String quelEestTonNoM()      								// Son nom commence par Sh�rif ; on r�definit donc la m�thode 
	   {
		return   " Sh�rif "  + nom  ;
	   }
	void sePresenter()    										//Il aime se pr�senter  on d�finit donc ici une m�thode 
		{
		super.sePresenter();
		parler(" Je suis "  + caracteristique +  "  et ma popularit� et de " + popular );
		parler(" J'ai d�j� arret� " + stopBrigand + " Brigand "); 							// Nombre de brigand arreter 
		}
	/***********Fin Exercice 8.1***********/	
	/***********Exercice 8.3*************/ 														//D�finition de la m�thode pour arreter les Brigands
	void coffrer(Brigand b)
	   {
		parler ( " Au nom de la loi, je vous arr�te," +  b.quelEestTonNoM()    +   " ! ");
		b.emprisonner(this); 																	// Appel de la m�thode qui fait reciter la phrase
		stopBrigand = 1 ; 																		//Le nombre de brigand arret� augmente
		System.out.println("");
		super.sePresenter();
		parler(" Je suis "  + caracteristique +  "  et ma popularit� et de " + popular );
		parler(" J'ai d�j� arret� " + stopBrigand + " Brigand "); 							// Nombre de brigand arreter 
	   }
	/***********Fin Exercice 8.3***********/
}
