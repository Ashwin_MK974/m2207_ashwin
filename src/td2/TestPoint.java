package td2;

public class TestPoint {

	public static void main(String[] args)

	{
		
		Point p; 
		p = new Point(7,2); // Instanciation de l'objet 
		
		
		p.getX(); // on applique getX sur p ce qui va retourner la valeur de 7 
				 // getX est un accesseur qui  prend la valeur X des coordon�es est la retourne
		p.getY();
		
		System.out.println(" X = " +  p.getX() + " et y = " +  p.getY());
		
		
		p.setX(5); // On donne comme valeur 5 � la m�thode setX car on peut modifier sa valeur dans la m�thode set
		p.setY(5);
		System.out.println("Les valeur modifi�e sont " + " x = " + p.getX() + " et y  = " +   p.getY());
		
		
		//Exercice 1.4
		System.out.println("Exercice 1.4");
		System.out.println(" x = " + p.x);
		System.out.println(" y = " + p.y);
		
		
		//Exercice 1.58
		p.move(-2, 3);
		
		System.out.println(p.getX());
		System.out.println(p.getY());  // 3 et  5 
	}

}
