package td3;

public class TestAnimal
{

	public static void main(String[] args) // Car il s'agit d'un programme c'est le point d'entr�es
	
	{
		
		//Exercice 1.2
		Animal animal = new Animal();    // Cr�ation de l'objet
		
		
		//Afiche le contenu de la chaine de caractere retourne par la methode
		System.out.println(animal.toString());
		// Affiche @15db9742 id unique de de l'objet Animale 
		
		// Fin Exercice 1.2 
		
		//Exercice 1.5
		
		 // Valeur renvoy�es par la m�thode affiche
		System.out.println(animal.Affiche());
		 // Valeurs renvoy� par la metthode cry
		System.out.println(animal.cri());
		
		// Fin exercie 1.5
		
		
		//Exercice 2.2
		 Chien rex = new Chien() ;  // Le chien sait faire ce que fait l'animale
		 
		 // Valeur renvoy�es par la m�thode affiche
		 	System.out.println(rex.Affiche()); // Va chercher la m�thode dans chien car la m�thode
		 									   // pour l'objet est d�finie si elle n'est pas 
		 										// elle recherche la premiere 
		 // Valeur renvoy�es par la m�thode cri
		 	System.out.println(rex.cri());
		 // Fin exercice  2.2 et 2.5 
		
		 // Exercice 2.6 On constate qu'il y'a eu une redefiinition de la m�thode il ne vas plus chercher la 
		 // M�thode par dafauts qui est affiche mais la m�thode applique  � instance chien 
		 	
		 	// Exercice 2.7 Renvoie de la m�thode sur plusieurs objets
		 	System.out.println(rex.origine());
		 		// Rex.originine n'existe pas dans la classe Chien car elle n'est pas d�finie
		 		// elle le cherche alors dans  la classe m�re Animale dans laquelle elle est d�inie 
		 	System.out.println(animal.origine());
		 	
		 	//Exercice 2.8 
		 		//Erreur de compilation car il y'a le mot cl� final qui interdit aux autre classe 
		 		// de redefinir cette classe .
		 	// Fin exercice 2.8
		 	
		 	
		 	
		 	  //Exercice 3.3
		 			Chat felix = new Chat();
		 			System.out.println(felix.affiche());
		 			System.out.println(felix.miauler());
		 			System.out.println(felix.cri()); // Vas pas trouver le cri il va 
		 											 // donc chercher dans la classe m�re
		 											// Ce qui va donc afficher ...
		 	
		 		//Fin exercice 3.3 
		
		 		//Exercice 3.4 
		 			//System.out.println(chat.miauler()); // Il n'y a pas de d�finition 
		 												// donc erreeur de compilation
		
		 		//Fin Exercice 3.4 		
		
		
		
		

	}

}
