package tp5;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
public class CompteurDeClic extends java.applet.Applet {

	int Compteur = 0;
    Button Bplus;
    Label Lab_compteur;
    Panel ligne1 = new Panel();
    Panel ligne2 = new Panel();
    public void init() 
    {
    	//* On ajoute deux ligne qui contiendra les deux element
        add(ligne1);				//Les increment et <=>
        add(ligne2);				//Le compteur
       /* on d�finit les boutons et on les pose dans la ligne 1*/
        Bplus = new Button("CLICK ! ");					// Bouton pour l'incr�mentation
        ligne1.add(Bplus);								//Ajout sur la ligne 1 

        Lab_compteur = new Label("  Vous avez cliquer "+  Compteur  + "    fois  "); //  Compte d'indication  par l'utilisation du label
        ligne2.add(Lab_compteur); 						// On ajoute le compteur sur la ligne n�2 
       //Fonction qui permet d'�couter les actions que l'on effectue sur le bouton
        ListenActionBoutton  EB= new    ListenActionBoutton();
        Bplus.addActionListener(EB);    //Ecoute des actions
    }
    //Accesseur pour retourner la valeur du compter
    public int getCompteur() 
    {
        return Compteur;
    }
    class    ListenActionBoutton implements java.awt.event.ActionListener //Algorithme pour incrementer le compteur
    	{
    		public void actionPerformed(ActionEvent e) {
    		if (e.getSource() == Bplus) // si action correspond a Bplus 
                Compteur++;				// On incremente le compteur
            else
                Compteur+=0;			//Si il n'y a pas d'action le compteur vaut  
            Lab_compteur.setText(" Vous avez cliquer " + String.valueOf(Compteur) + " fois ! "); // envoie des modification vers le label
            }
        }
    }