package tp4;

public class TestPokemon 
{
public static void main(String[] args)
{
		// TODO Auto-generated method stub
	
		/***********Exercice 1.4**************/	
		Pokemon p1 = new Pokemon("PikaLaZer");
		p1.sePresenter();                       		//Appel de la m�thode sePr�senter 
		/***********Fin Exercice 1.4***********/	
		/***********Exercice 1.5************/
		p1.manger();									//Appel de la m�thode manger 
		/***********Fin Exercice 1.5***********/
		/***********Exercice 1.6***********/
		p1.vivre();               						//Appel de la m�thode vivre
		/***********Fin Exercice 1.6***********/
		/***********Exercice 1.7***********/
		p1.isAlive();									//Appel de la m�thode isAlive 
		/***********Fin Exercice 1.7***********/
		/***********Exercice 1.8***********/			//Programme pour tester la dur�e de vie d'un Pok�mon 
		System.out.println("");
		int cycle = 0 ;									//On d�clare le cycle de vie
		while(p1.isAlive()==true)
		{
			p1.manger();
			p1.vivre();
			cycle ++ ; 
		}
		System.out.println(" Exercice 1.8 ");
		System.out.println(" PikaLaZer  � v�cu " + cycle + " cycle ");
		/***************************************
		/***********Fin Exercice 1.8***********/
}
}